# Anonymous Vote Network

This repository contains implementation of _Anonymous vote network_ which solves _Dining Cryptographers Problem_.

## Setup
To run this project you need Go in version 1.11 or higher. It need to supprot Go modules.
Run 3 separate clients in 3 different terminals:
```bash
#!/bin/bash

# terminal 1
go run main.go -n 0

# terminal 2
go run main.go -n 1

# terminal 3
go run main.go -n 2
```

Every client should display messages from rest of clients.
It's only PoC - every client connects to each other using TLS.
In next iteration full implementation of anonymous veto network will be added.

## Communication protocol
1. Establish connections  with every other user
1. Send public key to every other user
1. Wait for keys from every other user
1. Compute vote value
1. Send vote value to every other user
1. Wait for votes from every other user
1. Compute final result
1. Close connections

## Implemantation schema
* 

# Paper 
[link](https://www.overleaf.com/project/5eda7b13b060950001e494d6)