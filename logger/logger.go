package logger

import "log"

const (
	prover         string = "\u001b[34mProver\u001b[0m"
	verifier       string = "\u001b[33mVerifier\u001b[0m"
	formatReceived string = "%s received \u001b[36m%s\u001b[0m equal to \u001b[32m%s\u001b[0m"
	formatSent     string = "%s sent \u001b[36m%s\u001b[0m equal to \u001b[32m%s\u001b[0m"
	format         string = "%s %s"
)

// LogVerivierReceived logs value received by client.
func LogVerivierReceived(key, value string) {
	log.Printf(formatReceived, verifier, key, value)
}

// LogVerifierSent logs value sent by client.
func LogVerifierSent(key, value string) {
	log.Printf(formatSent, verifier, key, value)
}

// LogProverReceived logs value received by server.
func LogProverReceived(key, value string) {
	log.Printf(formatReceived, prover, key, value)
}

// LogProverSent logs value sent by server.
func LogProverSent(key, value string) {
	log.Printf(formatSent, prover, key, value)
}
