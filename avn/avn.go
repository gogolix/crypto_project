package avn

import (
	"math/big"
)

// Participant data
type Participant struct {
	S       Schnorr
	PrivKey *big.Int
	PubKey  *big.Int
}

// Message sent by channel
type Message struct {
	Public  []byte
	Private []byte
	G       []byte
}

// ComputeKeys computes keys
func (p *Participant) ComputeKeys() {
	p.S.GenerateSchnorrGroup()

	p.PrivKey = p.S.RandomValue()

	p.PubKey = big.NewInt(0)
	p.PubKey.Exp(p.S.G, p.PrivKey, p.S.P)
}
