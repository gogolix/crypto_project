package main

import (
	"fmt"
	"log"
	"math/big"
	"path/filepath"
	"project/avn"
	"project/client"
	"project/config"
	"project/server"
)

// N to Twoja stara
const N = 3

func main() {

	var params = config.Params{}
	params.ParseAgrs()

	clientsPorts := params.GetClientsPorts()
	clientsCount := len(clientsPorts)

	portIdx := params.GetPortIndex()

	if portIdx > 2 {
		log.Fatal("Only 3 participants allowed")
	}

	certAbsPath, err := filepath.Abs(fmt.Sprintf("./config/certs/server%d.crt", portIdx))
	if err != nil {
		log.Fatal(err)
	}

	keyAbsPath, err := filepath.Abs(fmt.Sprintf("./config/keys/server%d.key", portIdx))
	if err != nil {
		log.Fatal(err)
	}

	sendingChannel := make(chan avn.Message)

	participant := avn.Participant{}
	participant.ComputeKeys()

	s := server.Server{
		Port:           params.Port,
		CertPath:       certAbsPath,
		KeyPath:        keyAbsPath,
		SendingChannel: sendingChannel,
		Schnorr:        participant.S,
		ClientsCount:   clientsCount,
	}

	go s.Run()

	receivingChannels := make([]chan []byte, clientsCount)
	for i := 0; i < len(receivingChannels); i++ {
		receivingChannels[i] = make(chan []byte)
	}
	for i := 0; i < len(receivingChannels); i++ {
		c := client.Client{
			Port:             clientsPorts[i],
			ReceivingChannel: receivingChannels[i],
			Schnorr:          participant.S,
		}

		go c.Run()
	}

	pubKeyMarshalled, _ := participant.PubKey.MarshalText()
	privKeyMarshalled, _ := participant.PrivKey.MarshalText()
	gMarshalled, _ := participant.S.G.MarshalText()

	msg := avn.Message{
		Public:  pubKeyMarshalled,
		Private: privKeyMarshalled,
		G:       gMarshalled,
	}

	// Round 1.
	sendingChannel <- msg

	otherKeys := big.NewInt(1)
	for j := 0; j < portIdx; j++ {
		otherKey := &big.Int{}
		otherKey.UnmarshalText(<-receivingChannels[j])
		otherKeys.Mul(otherKeys, otherKey)
		otherKeys.Mod(otherKeys, participant.S.P)
	}
	for j := portIdx; j < len(receivingChannels); j++ {
		otherKey := &big.Int{}
		otherKey.UnmarshalText(<-receivingChannels[j])
		otherKeys.Mul(otherKeys, otherKey.ModInverse(otherKey, participant.S.P))
		otherKeys.Mod(otherKeys, participant.S.P)
	}

	// Round 2.
	vote := params.VoteValue
	c := &big.Int{}

	if vote == false {
		c.Set(participant.PrivKey)
	} else {
		for {
			c.Set(participant.S.RandomValue())
			if c.Cmp(participant.PrivKey) != 0 {
				break
			}
		}
	}

	myVote := &big.Int{}
	myVote.Exp(otherKeys, c, participant.S.P)

	myVoteMarshalled, _ := myVote.MarshalText()
	cMarshalled, _ := c.MarshalText()
	gYiMarshalled, _ := otherKeys.MarshalText()

	msg = avn.Message{
		Public:  myVoteMarshalled,
		Private: cMarshalled,
		G:       gYiMarshalled,
	}

	sendingChannel <- msg

	resultingVote := &big.Int{}
	resultingVote.Set(myVote)
	for j := 0; j < len(receivingChannels); j++ {
		otherVote := &big.Int{}
		otherVote.UnmarshalText(<-receivingChannels[j])

		resultingVote.Mul(resultingVote, otherVote)
		resultingVote.Mod(resultingVote, participant.S.P)
	}

	if resultingVote.Cmp(big.NewInt(1)) == 0 {
		log.Println("### \u001b[34mNoone vetoed!\u001b[0m ###")
	} else {
		log.Println("### \u001b[34mSomeone vetoed!\u001b[0m ###")
	}

	fmt.Scanln()
}
