package client

import (
	"crypto/tls"
	"fmt"
	"log"
	"math/big"
	"math/rand"
	"net"
	"project/avn"
	"project/logger"
	"time"
)

// Client struct
type Client struct {
	Port             string
	ReceivingChannel chan []byte
	Schnorr          avn.Schnorr
	t                *big.Int
}

// Run the client
func (c *Client) Run() {
	log.SetFlags(log.Lshortfile)
	log.Println("Client is connecting to port ", c.Port)

	conf := &tls.Config{
		InsecureSkipVerify: true,
	}

	var conn net.Conn
	var err error
	for {
		conn, err = tls.Dial("tcp", fmt.Sprintf("127.0.0.1:%s", c.Port), conf)
		if err != nil {
			time.Sleep(time.Second * 3)
			log.Println("Waiting for connection to server", c.Port)
			continue
		}

		break
	}
	defer conn.Close()

	log.Println("Client connected to port ", c.Port)

	c.t = big.NewInt(20)

	buf := make([]byte, 100)

	for {
		// Actual bufor length.
		var n int
		// Error which could occur during reading received value
		var err error

		// Read value received from other participant
		n, err = conn.Read(buf)
		if err != nil {
			log.Fatal(err)
		}
		// Value received from other participant.
		// Should be verified in next steps.
		recBytes := make([]byte, 100)
		copy(recBytes, buf[:n])

		// Received bytes converted to big.Int.
		publicValue := &big.Int{}
		publicValue.UnmarshalText(recBytes)
		logger.LogVerivierReceived("public value", publicValue.String())

		c.validateZKP(conn, publicValue)

		c.ReceivingChannel <- recBytes
	}
}

func (c *Client) validateZKP(conn net.Conn, publicValue *big.Int) {
	// Actual bufor length.
	var n int
	// Error which could occur during reading received value
	var err error
	// Message to sent.
	var msg []byte

	buf := make([]byte, 100)
	n, err = conn.Read(buf)
	if err != nil {
		return
	}

	// Valu which should be verified
	valueToVerify := &big.Int{}
	valueToVerify.UnmarshalText(buf[:n])
	logger.LogVerivierReceived("value to verify", valueToVerify.String())

	// Upper bound for get random value of c which should be sent to server.
	upperBound := &big.Int{}
	upperBound.Exp(big.NewInt(2), c.t, nil)

	// Get random value of C.
	randomC := &big.Int{}
	randomC.Rand(rand.New(rand.NewSource(time.Now().UnixNano())), upperBound)
	logger.LogVerifierSent("c", randomC.String())

	msg, err = randomC.MarshalText()

	if err != nil {
		log.Fatal(err)
	}

	// Send C to server.
	conn.Write(msg)

	// Get r value from server.
	n, err = conn.Read(buf)
	if err != nil {
		log.Fatal(err)
	}

	r := &big.Int{}
	r.UnmarshalText(buf[:n])
	logger.LogVerivierReceived("r", r.String())

	// Get base of exponent from server
	n, err = conn.Read(buf)
	if err != nil {
		log.Fatal(err)
	}

	g := &big.Int{}
	g.UnmarshalText(buf[:n])
	logger.LogVerivierReceived("g", g.String())

	// Verify value
	verified := &big.Int{}
	verified.Exp(g, r, c.Schnorr.P)
	verified.Mul(verified, publicValue.Exp(publicValue, randomC, c.Schnorr.P))
	verified.Mod(verified, c.Schnorr.P)

	if valueToVerify.Cmp(verified) != 0 {
		log.Fatal("\u001b[31mVerification failed!\u001b[0m")
	} else {
		log.Println("\u001b[36mValue sucesfully verified!\u001b[0m")
	}
}
