package avn

import (
	"math/big"
	"math/rand"
	"time"
)

// Schnorr contains Schnorr group params
type Schnorr struct {
	P *big.Int
	Q *big.Int
	R *big.Int
	H *big.Int
	G *big.Int
}

// GenerateSchnorrGroup generates Schnorr group
func (s *Schnorr) GenerateSchnorrGroup() {
	s.Q = &big.Int{}
	s.Q.SetString("341948486974166000522343609283189", 10)
	s.R = &big.Int{}
	s.R.SetString("338624364920977752681389262317185522840540224", 10)

	s.P = big.NewInt(23)
	s.P.Mul(s.Q, s.R)
	s.P.Add(s.P, big.NewInt(1))

	s.H = &big.Int{}
	s.H.SetString("3141592653589793238462643383279502884197", 10)

	s.G = big.NewInt(1)
	s.G.Exp(s.H, s.R, s.P)
}

// RandomValue generates random value
func (s *Schnorr) RandomValue() (r *big.Int) {
	exp := big.NewInt(0)
	exp.Rand(rand.New(rand.NewSource(time.Now().UnixNano())), big.NewInt(50000))

	r = big.NewInt(2137)
	r.Exp(s.G, exp, s.P)

	return r
}
