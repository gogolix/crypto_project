package config

import (
	"flag"
	"log"
	"sort"
	"strings"
)

// Params contains CLI args.
type Params struct {
	VoteValue         bool
	Port              string
	ParticipantsPorts string
}

// ParseAgrs parses CLI arguments
func (p *Params) ParseAgrs() {
	flag.BoolVar(&p.VoteValue, "v", false, "value of vote of the participant")
	flag.StringVar(&p.Port, "p", "8000", "value of port")
	flag.StringVar(&p.ParticipantsPorts, "u", "8001,8002", "list of port numbers of other particpiants")

	flag.Parse()
}

// GetClientsPorts returns parsed and validated values of clients' ports.
func (p *Params) GetClientsPorts() (ports []string) {
	for _, port := range strings.Split(p.ParticipantsPorts, ",") {

		if port == p.Port {
			log.Fatal("Other participant can't use the same port as current user")
		}

		ports = append(ports, port)
	}

	return ports
}

// GetPortIndex gets current user port index.
func (p *Params) GetPortIndex() int {
	ports := p.GetClientsPorts()
	ports = append(ports, p.Port)

	sort.Strings(ports)

	for idx, port := range ports {
		if port == p.Port {
			return idx
		}
	}

	return 0
}
