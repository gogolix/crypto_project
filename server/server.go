package server

import (
	"crypto/tls"
	"fmt"
	"log"
	"math/big"
	"net"
	"project/avn"
	"project/logger"
)

// Server struct
type Server struct {
	Port           string
	CertPath       string
	KeyPath        string
	SendingChannel chan avn.Message
	Schnorr        avn.Schnorr
	ClientsCount   int
}

// Run the server
func (s *Server) Run() {
	log.SetFlags(log.Lshortfile)

	cer, err := tls.LoadX509KeyPair(s.CertPath, s.KeyPath)

	if err != nil {
		log.Fatal(err)
	}

	config := &tls.Config{Certificates: []tls.Certificate{cer}}
	ln, err := tls.Listen("tcp", fmt.Sprintf(":%s", s.Port), config)
	if err != nil {
		return
	}
	defer ln.Close()

	handlerChannels := make([]chan avn.Message, s.ClientsCount)

	for i := 0; i < s.ClientsCount; i++ {
		handlerChannels[i] = make(chan avn.Message)
	}

	for i := 0; i < s.ClientsCount; i++ {
		conn, err := ln.Accept()
		if err != nil {
			continue
		}
		go handleConnection(conn, s.Schnorr, handlerChannels[i])
	}

	multiplexChannels(s.SendingChannel, handlerChannels)

	for i := 0; i < s.ClientsCount; i++ {
		close(handlerChannels[i])
	}
}

func multiplexChannels(in chan avn.Message, out []chan avn.Message) {
	for m := range in {
		for i := 0; i < len(out); i++ {
			out[i] <- m
		}
	}
}

func handleConnection(conn net.Conn, s avn.Schnorr, sendingChannel chan avn.Message) {
	defer conn.Close()
	for chanMsg := range sendingChannel {
		// Error which could occurr dugin writing to client or other operation.
		var err error
		// Message sent to other participants
		var msg []byte

		// Send public value to verfieir.
		// It could be public key of prover or value of prover's vote.
		_, err = conn.Write(chanMsg.Public)
		if err != nil {
			log.Fatal(err)
		}

		G := &big.Int{}
		G.UnmarshalText(chanMsg.G)

		v := s.RandomValue()
		v.Mod(v, s.Q)

		// Compute V which should be verified by verifier.
		V := &big.Int{}
		V.Exp(G, v, s.P)
		logger.LogProverSent("V", V.String())

		// Send V to verifier
		msg, err = V.MarshalText()
		if err != nil {
			log.Fatal(err)
		}

		_, err = conn.Write(msg)
		if err != nil {
			log.Fatal(err)
		}

		buf := make([]byte, 100)
		n, err := conn.Read(buf)
		if err != nil {
			log.Fatal(err)
		}

		// Get c from verifier
		c := &big.Int{}
		c.UnmarshalText(buf[:n])
		logger.LogProverReceived("c", c.String())

		a := &big.Int{}
		a.UnmarshalText(chanMsg.Private)

		// Compute r for verifier
		r := &big.Int{}
		r.Sub(v, a.Mul(a, c))
		r.Mod(r, s.Q)
		logger.LogProverSent("r", r.String())

		msg, err = r.MarshalText()

		// Send r to verifier
		_, err = conn.Write(msg)
		if err != nil {
			log.Fatal(err)
		}

		// Send exponent base to other participant
		_, err = conn.Write(chanMsg.G)
		if err != nil {
			log.Fatal(err)
		}

		g := &big.Int{}
		g.UnmarshalText(chanMsg.G)
		logger.LogProverSent("g", g.String())
	}
}
